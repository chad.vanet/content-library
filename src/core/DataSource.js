let instance = null;
/**
 * The global datasource instance for use by jsonp
 * @type {string}
 */
const DataSourceGlobal = "window.SB.Data";

/**
 * To assist with switchboard data handling on local and remote deployments
 * @note This class mutates the Switchboard.dataSource object
 * It assigns data sources to their alias as well as adds a meta attribute
 * where info about the data source resides
 */
class DataSource {

  /**
   * @see instance()
   */
  constructor(core) {
    if (!instance) {
      this._core = core;
      this._sources = [];
      this._completeCallback = null;
      this._jsonpCallbacks = { i:0 };

      this._global = DataSourceGlobal;

      this.base = "";
      this.location = "";

      instance = this;
    }
    return instance;
  }

  /**
   * Get the singleton instance of DataSource
   * @return {DataSource} The DataSource singleton
   */
  static instance() {
    return new DataSource();
  }

  /**
   * Load data from switchboard for local development
   * @param  {object}   config   See example for full configuration
   *
   * @example
   * SB.Data.load({
   * 	url:'http://demo.coatesdigital.com.au',
   * 	sources: [
   * 		{ name:'config', filename:'basic-menu-config.csv' },
   * 		'basic-product-group-nutrition.csv'
   * 	],
   * 	success: function () {
   * 		// start application
   * 	}
   * });
   */
  load(config) {
    this._completeCallback = config.success || function () {};

    this.base = config.url || '';
    this.location = config.location || '';
    this._sources = config.sources || [];

    if (this._core.local &&
        this._sources.length > 0) {
      window.Switchboard.dataSources = {};

      // get each source map
      this._sources.map(s => {
        let source = typeof(s) == 'string' ? s : s.filename;
        let name = typeof(s.name) != 'undefined' ? s.name : source;
        let datasource = this._toAPIFormat(this.base, this.location) + source + '.jsonp';

        // setup callback
        let jsonpName = "fn" + this._jsonpCallbacks.i++;
        this._jsonpCallbacks[jsonpName] = ((dn, fn) => {
          let that = this;
          return function () {
            delete that._jsonpCallbacks[fn];

            let args = Array.prototype.slice.call(arguments);
            args.unshift(dn);
            that._complete.apply(that, args);
          };

        })(name, jsonpName);

        // get data via jsonp
        this._jsonp(datasource, jsonpName);
      });
    } else {
      // compatability
      if (this._core.compatability) {
        window.Switchboard.dataSources = window.data;
      }
      //
      this._completeCallback();
    }
  }

  /**
   * Load data from switchboard for local and hq environments
   * @param {string}    filename  The filename of the dataSource on hq
   * @param {string}    name      The alias for the dataSource
   * @param {Boolean}   reload    Reload the dataSource
   * @return {Promise}  A promise that returns the dataSource data
   *
   * @example
   * SB.Data.fetch('basic-products.csv', 'products', true).then(function (data) {
   *   // basic-products.csv data
   *   console.log(data);
   *   // basic-products.csv data
   *   console.log(SB.Data.get('products'));
   * });
   */
  fetch(filename, name, reload) {
    var source = name || filename;
    // source exists and don't reload it
    if (this.has(source) && !reload) {
      return Q.when(this.get(source));
    }

    // if no source or reload, load it
    var deferred = Q.defer();
    let datasource = this._toAPIFormat(this.base, this.location) + source + '.jsonp';

    // setup callback
    let jsonpName = "fn" + this._jsonpCallbacks.i++;
    this._jsonpCallbacks[jsonpName] = ((dn, fn, filename) => {
      let that = this;
      return function () {
        delete that._jsonpCallbacks[fn];

        let args = Array.prototype.slice.call(arguments);
        args.unshift(dn);

        // add source to switchboard object
        let name = args[0];
        let data = args[1];

        window.Switchboard.dataSources[name] = data;

        that._sources.push({
          name: name,
          filename: filename
        });

        // resolve source
        deferred.resolve(data);
      };

    })(source, jsonpName);

    // get data via jsonp
    this._jsonp(datasource, jsonpName);
    return deferred.promise;
  }

  /**
   * Load several dataSources from switchboard for local and hq environments
   * @param {Array}     dataSources The collection of dataSources to load
   * @param {Boolean}   reload      Reload the dataSources
   *
   * @example
   * SB.Data.fetchAll([
   * 		{ name:'config', filename:'basic-menu-config.csv' },
   * 		'basic-product-group-nutrition.csv'
   * 	], true).then((data) => {
   *    // array of data sources
   *    console.log(data);
   *  });
   */
  fetchAll(dataSources, reload = false) {
    var promises = [];

    dataSources.forEach((item) => {
      item = typeof(s) === 'string' ? { filename: item } : item;
      promises.push(this.fetch(item.filename, item.name, reload));
    });

    return Q.all(promises);
  }

  /**
   * Get a data source from Switchboard
   * @param  {string} name Name of file or custom name
   * @return {Array}      Data from Switchboard
   *
   * @example
   * SB.Data.load({
   * 	url:'http://demo.coatesdigital.com.au',
   * 	sources: [
   * 		{ name:'config', filename:'basic-menu-config.csv' },
   * 		'basic-products.csv'
   * 	],
   * 	success: function () {
   * 		SB.Data.get('config'); // returns basic-menu-config.csv
   *   	SB.Data.get('basic-menu-config.csv'); // returns the same
   * 	  SB.Data.get('basic-products.csv'); // returns basic-products.csv
   * 	  SB.Data.get('not-a-data-source'); // throws error
   * 	}
   * });
   */
  get(name) {
    this._assignMeta();

    // if name isn't given, return all
    if (typeof name === 'undefined') {
      var data = this._toArray(window.Switchboard.dataSources);
      if (typeof data === 'undefined')
        data = {};
      return this._addMethods(data);
    }

    // exists with name
    if (typeof(window.Switchboard.dataSources[name]) !== 'undefined') {
      return this._toArray(window.Switchboard.dataSources[name]);
    }

    // find source with name and use filename
    for (let i = 0; i < this._sources.length; i++) {
      let s = this._sources[i];

      if (typeof(s) === 'string') continue;
      if (name == s.name &&
          typeof(window.Switchboard.dataSources[s.filename]) !== 'undefined') {
        return this._toArray(window.Switchboard.dataSources[s.filename]);
      }
    }

    // nothing exists
    throw new Error('DataSource "' + name + '" does not exist in Switchboard.dataSource');
  }

  /**
   * Get a datasource with name like name
   * @param  {String} name The name to compare
   * @return {object}      All matching datasources
   */
  like(name) {
    return this.get().like(name);
  }

  /**
   * Update a data source with new data
   * @param {update} name The name of the data source to update
   * @param {Array} data The data to store
   *
   * @example
   * SB.Data.load({
   * 	url:'http://demo.coatesdigital.com.au',
   * 	sources: [
   * 		{ name:'prices', filename:'basic-prices.csv' },
   * 		{ name:'products', filename:'basic-products.csv' }
   * 	],
   * 	success: function () {
   * 		var joinedDataset = SB.Dataset(SB.Data(products))
   * 				.join(SB.Data(prices)).on('id')
   * 			 	.get();
   *    SB.Data.set('products', joinedDataset); // overwrite with the newly combined dataset
   * 	}
   * });
   */
  set(name, data) {
    if (typeof(window.Switchboard.dataSources[name]) === 'undefined') {
      let notFound = name;
      // find source with name and use filename
      for (let i = 0; i < this._sources.length; i++) {
        let s = this._sources[i];

        if (typeof(s) === 'string') continue;
        if (name == s.name &&
            typeof(window.Switchboard.dataSources[s.filename]) !== 'undefined') {
            name = s.filename;
        }
      }
    }

    window.Switchboard.dataSources[name] = data;
  }

  /**
   * Verify a data source exists
   * @param  {string}  name Name of file or custom name
   * @return {Boolean}      Data source exists
   *
   * @example
   * SB.Data.load({
   * 	url:'http://demo.coatesdigital.com.au',
   * 	sources: [
   * 		{ name:'config', filename:'basic-menu-config.csv' },
   * 		'basic-products.csv'
   * 	],
   * 	success: function () {
   * 	 SB.Data.has('config'); // returns true
   * 	 SB.Data.has('basic-menu-config.csv'); // returns true
   * 	 SB.Data.has('not-a-data-source'); // returns false}
   * });
   */
  has(name) {
    try {
      this.get(name);
    } catch (e) {
      return false;
    }
    return true;
  }

  /**
   * Get a datasource with name like name
   * @param  {String} name The name to compare
   * @param  {Array}  data The datasources to match against
   * @return {object}      All matching datasources
   */
  _like(name, data) {

    var datasources = {};
    for (var prop in data) {
      if (prop.toLowerCase().indexOf('.csv') > 0 &&
          prop.toLowerCase().indexOf(name.toLowerCase()) >= 0)
        datasources[prop] = data[prop];
    }

    return this._addMethods(datasources);
  }

  /**
   * Get the most recently scheduled datasource
   * @param  {Array}  data   The datasources to match against
   * @return {object}        The closest scheduled datasource or the single one
   */
  _scheduled(data) {
    var offset = new Date().getTimezoneOffset() * 60000; // offset in milliseconds
    var now = Date.now();

    var min = null, minKey = null;
    var pattern = /\d{8}(T\d{6}Z*)*(\.csv)/g;

    for (var prop in data) {
      // get data section using format
      if (prop.match(pattern)) { // has a date in the name
          // pull date from name and convert to standard
          var nd = prop.match(pattern)[0].replace('.csv', '');
          var iso8601 = nd.substring(0,4) + '-' +
                        nd.substring(4,6) + '-' +
                        nd.substring(6,8);

          if (nd.indexOf('T') > 0) {
            iso8601 += nd.substring(8,11) + ':' +
                       nd.substring(11,13) + ':' +
                       nd.substring(13);

            if (nd.indexOf('Z') < 0) {
              iso8601 += 'Z';
            }
          } else {
            iso8601 += "T00:00:00Z";
          }

          // create date to compare
          var date = new Date(iso8601).getTime() + offset;

          // find closest previous date
          if (date <= now &&
              (min == null || min < date)) {
            min = date;
            minKey = prop;
          }
      }
    }

    if (minKey)
      return data[minKey];

    return data.single();
  }

  /**
   * Get the single datasource from Switchboard Datasources
   * @return {object} The single datasource object
   */
  single() {
    return this.get().single();
  }

  /**
   * Get the single csv object from the datasources
   * @return {object} The single datasource object
   */
  _single() {
    for (var prop in this) {
      if (prop.toLowerCase().indexOf('.csv') > 0)
        return this[prop];
    }
    return null;
  }

  /**
   * Add data and verify complete data load
   * @param  {string} name The name of the data source that has loaded
   * @param  {Array} data The data to store to the Switchboard object
   */
  _complete(name, data) {
    window.Switchboard.dataSources[name] = data;
    // verify all sources loaded
    let complete = true;
    for (let i = 0; i < this._sources.length; i++) {
      let s = this._sources[i];
      let source = typeof(s) == 'string' ? s : s.filename;
      let name = typeof(s.name) != 'undefined' ? s.name : source;

      if (typeof(window.Switchboard.dataSources[name]) === 'undefined') {
        complete = false;
      }
    }
    if (complete) {
      this._completeCallback();
    }
  }

  /**
   * Formats url to include api path
   * @param  {string} url The url to format
   * @return {string}     The formatted url
   */
  _toAPIFormat(url, location) {
    if (location) {
      if (url[url.length - 1] != '/')
        url += '/';
      url += `local/${location}/`;
    }

    if (url.indexOf('/api/dataSource/') < 0) {
      if (url[url.length - 1] != '/')
        url += '/';
      return url + 'api/dataSource/';
    }
  }

  /**
   * Generates a script element from parameters
   * @param  {string} src          The url to the data source
   * @param  {string} callbackName The function name to call on data load
   * @return {HTMLElement}              Script HTML Element
   */
  _toScriptTag(src, callbackName) {
    let script = document.createElement('script');
    script.src = `${src}?callback=${DataSourceGlobal}._jsonpCallbacks.${callbackName}`;
    script.dataset.switchboard = true;
    return script;
  }

  /**
   * Start JSONP
   * @param  {string} src          The url to the data source
   * @param  {string} callbackName The function name to call on data load
   */
  _jsonp(src, callbackName) {
    document.body.appendChild(this._toScriptTag(src, callbackName));
  }

  /**
   * Convert array-like data to an array if possible
   * @param  {object} data The data to convert
   * @return {Array}      The data as an Array
   */
  _toArray(data) {
    if (data instanceof Array)
      return Array.from(data);
    return data;
  }

  /**
   * Add fancy methods to data being returned
   * @param {Array} data Array to update
   */
  _addMethods(data) {
    data.like = (e) => { return this._like(e, data); };
    data.scheduled = () => { return this._scheduled(data); };
    data.single = this._single;
    return data;
  }

  /**
   * Loops through and assigns info to all data sources
   */
  _assignMeta() {
    for (var key in window.Switchboard.dataSources) {
      let filename = key;
      for (let i = 0; i < this._sources.length; i++) {
        let s = this._sources[i];
        let source = typeof(s) == 'string' ? s : s.filename;
        let alias = typeof(s.name) != 'undefined' ? s.name : source;

        if (key === alias) {
          filename = source;
        }
      }
      window.Switchboard.dataSources[key].meta = {
        name: key,
        filename: filename
      };
    }
  }
}

/**
 * Assists with loading and accessing Switchboard data
 */
export default DataSource;

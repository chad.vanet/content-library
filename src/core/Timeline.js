import TimelineDebug from '../debug/TimelineDebug';
import {Array} from './Polyfill';

/**
 * The default framerate of a timeline
 * @type {Number}
 */
const DefaultTimelineFramerate = 10;

/**
 * The default cycle time of a timeline
 * @type {Number}
 */
const DefaultTimelineCycleTime = 0;

/**
 * Coordinates events within a cycle based on system clock
 * @example
 * var sb = new SB();
 * sb.timeline.addEvent(function () {...}, 0, 100); // callback will run every 100 ms
 */
class Timeline {

  /**
   * Initialises a new timeline and begins ticking
   * @param  {object} config  Adjusts timeline behaviour
   */
  constructor(config = {}) {
    this._drawDebug = config.debug || false;
    this._timelineDebug = new TimelineDebug(this._drawDebug);

    this._framerate = config.framerate || DefaultTimelineFramerate;
    this._cycleTime = config.cycleTime || DefaultTimelineCycleTime;
    this._offset = config.offset || 0;

    this._timelineEvents = {};
    this._fixedEvents = [];
    this._intervalEvents = [];

    this._previous = 0;
    this._currentlyExecuting = null;

    this._frameLength = Math.floor(1000 / this._framerate);

    window.requestAnimationFrame(() => { this._update(); });
  }

  /**
   * Add an event, if interval parameter is given the event will repeat
   * @param {Function}  callback The function to call on event trigger
   * @param {Number}  time The start time of the first event trigger
   * @param {Number} interval The interval between event trigger
   */
  addEvent(callback, time, interval = null, tag) {
    if (interval == null)
      return this._addFixedEvent(callback, time, tag);
    else
      return this._addIntervalEvent(callback, time, interval, tag);
  }

  /**
   * Get the latest event to the current time
   * @param  {Number} time The time to step back from
   */
  triggerLatest(time = null) {
    if (time == null)
      time = this._cyclePosition;

    while (time >= 0) {
      if (this._timelineEvents[time]) {
        this._timelineEvents[time].forEach(e => e.trigger(time));
        time = 0;
      }

      time -= this._frameLength;
    }
  }

  /**
   * Clear current timeline events
   */
  clear() {
    this._timelineEvents = {};
  }

  /**
   * @deprecated
   * Perform cleanup operations
   */
  cleanUp() { }

  /**
   * Reset the timeline back to 0
   */
  reset() {
    this._offset = -Date.now();
  }

  /**
   * Set the total cycle time in milliseconds
   * @param  {Number} val The number of milliseconds
   */
  set cycleTime(val) {
    this._cycleTime = val;
    this._generate();
  }

  /**
   * Update the timeline clock and run any events
   */
  _update() {
    // store cycle position in local due to [Date mock limitation]
    let cyclePosition = this._cyclePosition;

    // if total animation time is 0, return
    if (this._cycleTime == 0) {
      return;
    }

    // get time from system clock
    if (cyclePosition != this._currentExecuting) {
      this._currentExecuting = cyclePosition;

      let previous = this._previous;
      for (let i = previous; i < cyclePosition; i++) {
        if (this._timelineEvents[i]) {
          this._timelineEvents[i].forEach(e => e.trigger(cyclePosition));
        }
      }
      this._previous = this._currentExecuting;

      // debug draw
      this._timelineDebug.update(cyclePosition, this._cycleTime, this._timelineEvents);
    }

    window.requestAnimationFrame(() => { this._update(); });
  }

  /**
   * Add an event that will play once per cycle
   * @param {Function}  callback The function to call on event trigger
   * @param {Number}  time The start time of the event trigger
   * @return {Object} The event that was created
   */
  _addFixedEvent(callback, time, tag) {
    let ev = new FixedTimelineEvent(callback, this._toFrameTime(time), tag)
    this._fixedEvents.push(ev);
    this._generate();
    return ev;
  }

  /**
   * Add an event that will play every {interval} seconds after {time}
   * @param {Function}  callback  The function to call on event trigger
   * @param {Number}  time  The start time of the first event trigger
   * @param {Number}  interval The interval between event trigger
   * @return {Object} The event that was created
   */
  _addIntervalEvent(callback, time, interval, tag) {
    let ev = new IntervalTimelineEvent(callback,
                                        this._toFrameTime(time),
                                        this._toFrameTime(interval),
                                        tag);
    this._intervalEvents.push(ev);
    this._generate();
    return ev;
  }

  /**
  * Generate the timeline from events
  */
  _generate() {
    if (this._cycleTime == 0) {
      return;
    }

    // clear timeline
    this.clear();

    // insert fixed events to timeline
    this._fixedEvents.forEach(e => {
      this._insertEvent(e.time, e);
    });

    // insert interval events to timeline
    this._intervalEvents.forEach(e => {
      let counter = e.time;
      while (counter < this._cycleTime) {
        this._insertEvent(counter, e);
        counter += e.interval;
      }
    });
  }

  /**
   * Insert event at time
   * @param {Number}  time The time to trigger the event
   * @param {TimelineEvent}  e The event to trigger
   */
  _insertEvent(time, e) {
    if (typeof(this._timelineEvents[time]) === 'undefined') {
      this._timelineEvents[time] = [];
    }
    this._timelineEvents[time].push(e);
  }

  /**
   * Get the current cycle position of timeline
   */
  get _cyclePosition() {
    let millisecs = Date.now();

    // offset milliseconds
    millisecs += this._offset;

    // check if any items to process from the timeline
    return this._toFrameTime(millisecs % this._cycleTime);
  }

  /**
   * round the time to the nearest frame time
   * @param  {Number} time The time to convert in milliseconds
   * @return {Number}      The frame for that time
   */
  _toFrameTime(time) {
    return Math.floor(time / this._frameLength) * this._frameLength;
  }
};

/**
 * Parent class of timeline events
 */
class TimelineEvent {
  /**
   * @param {Function} The function to call on event trigger
   * @param {Number} The time to trigger the event
   */
  constructor(callback, time, tag = '') {
    this._callback = callback;
    this._time = time;
    this._tag = tag;
  }

  /**
   * The time the event runs
   * @return {number} The time the event runs
   */
  get time() { return this._time; }

  /**
   * A tag describing the event
   * @return {string} The tag describing the event
   */
  get tag() { return this._tag; }

  /**
   * Executes the event callback
   */
  trigger(time) {
    this._callback(time);
  }
}

/**
 * Timeline event that triggers once per timeline loop
 */
class FixedTimelineEvent extends TimelineEvent {
  /**
   * @param {Function} callback The function to call on event trigger
   * @param {Number} time The time to trigger the event
   */
  constructor(callback, time, tag) {
    super(callback, time, tag);
  }
}

/**
 * Timeline event that triggers multiple times per timeline loop
 */
class IntervalTimelineEvent extends TimelineEvent {
  /**
   * @param {Function} callback The function to call an event trigger
   * @param {Number}  time The time to trigger the event
   * @param {Number}  interval Time til event repeat trigger
   */
  constructor(callback, time, interval, tag) {
    super(callback, time, tag);
    this._interval = interval;
  }

  /**
   * The number of milliseconds between triggers
   * @return {Number} The number of milliseconds between triggers
   */
  get interval() { return this._interval; }
}

/**
 * Performs event scheduling and syncing between switchboard interfaces
 */
export default Timeline;

import Dataset from './Dataset';
import Timeline from './Timeline';
import Data from './DataSource';
import Env from './Environment';
import QueryString from '../tool/QueryString';

const EnvLocal = "local";
const EnvSwitchboard = "switchboard";
const EnvSwitchboardOld = "old_switchboard";

const Environment = (typeof(window.Switchboard) === 'undefined') ?
                      ((typeof(window.data) === 'undefined') ? EnvLocal : EnvSwitchboardOld) : EnvSwitchboard;

// Instantiate Switchboard object
if (Environment === EnvLocal || Environment === EnvSwitchboardOld) {
  window.Switchboard = {};
}


let CoreLoaded = {
  data: false,
  env: false,
  init: false
};

let CoreDebug = 0;

let SetupDeferred = [];
let SetupCallbacks = [];

/**
 * Wraps the core components
 */
class Core {

  /**
   * Create a new switchboard
   */
  constructor(config = {}) {
    config.offset = config.offset || QueryString.get('localOffset');

    if (config.debug) {
      CoreDebug++;
      config.debug = CoreDebug;
    }

    this.Timeline = new Timeline(config);
  }

  /**
   * A single entry point for initiating the framework
   * @param  {object} config Config object containing all the details for the framework
   */
  static setup(config) {
    // if data is already loaded
    if (CoreLoaded.data && CoreLoaded.env) {
      if (config.success) {
        config.success();
      }
      return Q.when();
    }

    // else store callback / promise
    var deferred = Q.defer();
    SetupDeferred.push(deferred);
    if (config.success) {
      SetupCallbacks.push(config.success);
    }

    // if initial load has not been completed
    if (!CoreLoaded.init) {
      CoreLoaded.init = true;

      var resolveAll = () => {
        SetupCallbacks.forEach(callback => callback());
        SetupCallbacks = [];

        SetupDeferred.forEach(deferred => {
          deferred.resolve();
        });
        SetupDeferred = [];
      };

      // create configuration
      var datasource = {
        url: config.url || null,
        location: config.location || config.dataSourceLocation || null,
        sources: config.sources || null,
        success: () => {
          console.log("Datasources loaded.");
          CoreLoaded.data = true;
          if (CoreLoaded.env) {
            resolveAll();
          }
        }
      };

      config.env = config.env || {};

      var env = {
        url: config.url || null,
        location: config.location || config.environmentLocation || null,
        content: config.content || null,
        channelScreenId: config.channelScreenId,
        screenId: config.screenId,
        orientation: config.orientation,
        success: () => {
          console.log("Environment loaded.");
          CoreLoaded.env = true;
          if (CoreLoaded.data) {
            resolveAll();
          }
        }
      };

      // setup
      Core.Data.load(datasource);
      Core.Env.load(env);
    }

    return deferred.promise;
  }

  /**
   * The timeline object for this instance
   * @return {[type]} [description]
   */
  get timeline() {
    return this.Timeline;
  }

  /**
   * Is the current environment local
   * @return {Boolean}    Current Environment is local
   */
  static get local() {
    return Environment === EnvLocal;
  }

  /**
   * Is the current environment in compatability mode
   * @return {Boolean}    Current Environment is in compatibility
   */
  static get compatability() {
    return Environment === EnvSwitchboardOld;
  }

  /**
   * Dataset factory
   * @param {object} data The primary datasource to combine to
   * @return {Dataset}    New dataset object using data
   */
  static Dataset (data) {
    return new Dataset(data);
  }

  /**
   * Get DataSource singleton
   * @return {DataSource}   The DataSource singleton
   */
  static get Data() {
    return new Data(this);
  }

  /**
   * Get Environment singleton
   * @return {Environment}    The Environment singleton
   */
  static get Env() {
    return new Env(this);
  }

  /**
   * The timeline class for coordinating events according to system clock
   */
  static get Timeline() {
    return Timeline;
  }

  /**
   * Get QueryString tool
   */
  static get QueryString() {
    return QueryString;
  }

  /**
   * Exposes core components for global access (testing)
   * @param  {object} root Usually the window object
   */
  static _exposeTo(root) {
    root._sb = {};
    root._sb._dataset = Core.Dataset;
    root._sb._timeline = Timeline;

    root._sb._dataSource = Data;
    root._sb._environment = Env;

    root._sb._dataSourceInstance = Core.Data;
    root._sb._environmentInstance = Core.Env;

    root._sb._tool = {};
    root._sb._tool._queryString = Core.QueryString;

    root._sb._reset = function () {
      CoreLoaded = {
        data: false,
        env: false,
        init: false
      };

      CoreDebug = 0;

      SetupDeferred = [];
      SetupCallbacks = [];
    };
  }
};

/**
 * Wraps the core components
 */
export default Core;

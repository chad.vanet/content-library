let instance = null;

/**
 * The global datasource instance for use by jsonp
 * @type {String}
 */
const EnvironmentGlobal = 'window.SB.Env';

/**
 * To assist with switchboard environment data access on local and remote deployments
 */
class Environment {
    /**
     * @see instance()
     */
    constructor(core) {
      if (!instance) {
        this._core = core;
        instance = this;
      }
      return instance;
    }

    /**
     * Get the singleton instance of DataSource
     * @return {DataSource} The DataSource singleton
     */
    static instance() {
      return new Environment();
    }

    /**
     * Get the environments location data
     * @return {object} See example
     *
     * @example
     * {
     * 	id: "Test",
     * 	name: "New Location 1",
     * 	label: "Red",
     * 	timezone: "Australia/Sydney",
     * 	groups: [
     * 		"location-group-1",
     * 		"location-group-2"
     * 	]
     * }
     */
    get location() {
      return {
        id: this.get('location'),
        name: this.get('location-name'),
        label: this.get('location-label'),
        timezone: this.get('location-timezone'),
        groups: this.get('location-groups')
      };
    }

    /**
     * Get the orientation for this environment
     * @return {string} "vertical" | "horizontal"
     * @note "normal" === "vertical", "left" === "horizontal"
     */
    get orientation() {
      return this.get('orientation').toLowerCase() !== "normal" ? "vertical" : "horizontal";
    }

    /**
     * Get the screen sequence in the environment
     * @return {Number} the screens relative position in the environment, between 1-6
     */
    get sequence() {
      return parseInt(this.get('sequence'));
    }

    /**
     * Get the details for the content in this environment
     * @return {object} See example
     *
     * @example
     * {
     * 	name:"basic-coffee.zip",
     * 	label:"Red",
     * 	modified: "2016-01-17T19:18:54.000Z",
     * 	groups: [
     * 		"my-group",
     * 		"my-test-group"
     * 	]
     * }
     */
    get content() {
      return {
        name: this.get('content-name'),
        label: this.get('content-label'),
        modified: this.get('content-modified'),
        groups: this.get('content-groups')
      };
    }

    /**
     * Get channel details for environment
     * @return {object} See example
     *
     * @example
     * {
     * 	name: "My channel name",
     * 	screen: "",
     * 	screens: [],
     * 	groups: []
     * }
     */
    get channel() {
      return {
        name: this.get('channel-name'),
        screen: this.get('channel-screen'),
        screens: this.get('channel-screens'),
        groups: this.get('channel-groups')
      };
    }

    /**
     * Load environment data from switchboard for local development
     * @param  {object}   config   See example for full configuration
     *
     * @example
     * SB.Env.load({
     * 	url:'http://demo.coatesdigital.com.au',
     * 	location: 'TEST',
     * 	content:'basic-coffee.zip',
     * 	success: function () {
     * 		// start application
     * 	}
     * });
     */
    load(config) {
      this._completeCallback = config.success || function () {};

      let base = config.url || '';
      let location = config.location || '';
      let orientation = config.orientation || '';
      let channelScreenId = config.channelScreenId || null;
      let screenId = config.screenId || null;
      let content = config.content || '';
      this._sources = config.sources || [];

      if (this._core.local && location != '' && content == '') {
        console.warn('Location provided to environment but no content provided.');
      }

      if (this._core.local && content != '') {
        window.Switchboard.environmentData = {};

        // mock location using query string
        let query;
        let params = {
          'locationCode'    :location,
          'orientation'     :orientation,
          'channelScreenId' :channelScreenId,
          'screenId'        :screenId
        };
        let queryParams = [];
        Object.keys(params).forEach(e => {
          if (params[e]) {
              queryParams.push(e + '=' + params[e]);
          }
        });
        query = (queryParams.length > 0 ? '?' + queryParams.join('&') : "");

        // get environment data endpoint
        let dataEndpoint = this._toAPIFormat(base, location) + content + '.jsonp/' + query;
        // get data via jsonp
        this._jsonp(dataEndpoint);
      } else {
        //
        if (this._core.compatability) {
          window.Switchboard.environmentData = window.meta;
        }
        //
        this._completeCallback();
      }
    }

    /**
     * Get a data source from Switchboard
     * @param  {string} name Name of file or custom name
     * @return {*}      Environment data from Switchboard
     *
     * @example
     * SB.Env.load({
     * 	url:'http://demo.coatesdigital.com.au',
     * 	content: 'basic-coffee.zip',
     * 	success: function () {
     * 		SB.Env.get('content-name'); // return "basic-coffee.zip"
     *   	SB.Env.get('not-a-env-value'); // throw error
     * 	}
     * });
     */
    get(name) {
      // exists with name
      if (typeof(window.Switchboard.environmentData[name]) !== 'undefined')
        return window.Switchboard.environmentData[name];
      // return the lot
      if (typeof(name) === 'undefined')
        return window.Switchboard.environmentData;
      // nothing exists
      throw new Error('Environment Data does not exist in SB.Env');
    }

    /**
     * Verify a data source exists
     * @param  {string}  name Name of file or custom name
     * @return {Boolean}      Data source exists
     *
     * @example
     * SB.Env.load({
     * 	url:'http://demo.coatesdigital.com.au',
     * 	content: 'basic-coffee.zip',
     * 	success: function () {
     * 		SB.Env.has('content-name'); // return true
     *   	SB.Env.has('not-a-env-value'); // return false
     * 	}
     * });
     */
    has(name) {
      try {
        this.get(name);
      } catch (e) {
        return false;
      }
      return true;
    }

    /**
     * Add data and verify complete data load
     * @param  {string} name The name of the data source that has loaded
     * @param  {Array} data The data to store to the Switchboard object
     */
    _complete(data) {
      window.Switchboard.environmentData = data;
      this._completeCallback();
    }

    /**
     * Formats url to include api path
     * @param  {string} url The url to format
     * @return {string}     The formatted url
     */
    _toAPIFormat(url, location, orientation, channelScreenId, screenId) {
      if (url.indexOf('/api/environmentData/') < 0) {
        if (url[url.length - 1] != '/') {
          url += '/';
        }
        return url + 'api/environmentData/';
      }
    }

    /**
     * Generates a script element from parameters
     * @param  {string} src          The url to the data source
     * @param  {string} callbackName The function name to call on data load
     * @return {HTMLElement}              Script HTML Element
     */
    _toScriptTag(src, callback) {
      let script = document.createElement('script');
      let joinChar = src.indexOf('?') > 0 ? '&' : '?';
      script.src = `${src}${joinChar}callback=${EnvironmentGlobal}._complete`;
      script.dataset.switchboard = true;
      script.dataset.switchboardenv = true;
      return script;
    }

    /**
     * Start JSONP
     * @param  {string} src          The url to the data source
     * @param  {string} callback     The name of the callback
     */
    _jsonp(src, callback) {
      document.body.appendChild(this._toScriptTag(src));
    }
}

export default Environment;

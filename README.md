# Usage Documentation 1.0

----
## What's included?
### Core Framework
#### SB.Data
Data allows you to access your Switchboard data sources within your development environment and remotely. It also provides an interface to manage that data within your content.

#### SB.Dataset
Dataset allows you to manipulate your contents Switchboard data. Since it is a good idea to break up related data for use with Switchboard, Dataset gives you a quick and easy way to match related data in an easy to read SQL-like format.

> **Note:** SB 1.0 only supports `join` and `joinMany` functionality. If you think of a feature that you would like please add it to this repositories issues.

#### SB.Timeline
Timeline is used to schedule your events and synchronise actions based on the systems internal clock. You are able to run multiple timelines within one piece of content by instantiating a new Switchboard object.
```
// creates a board with a new timeline
var timeline = new SB.Timeline(); // a new timeline
```

#### SB.Env (Environment)
Environment provides easy access to the environment data related to the content being displayed.

----
## Getting Started
> **Note** The following will be a short tutorial showing the basics in order to get started with our framework.

To utilise the core Switchboard framework functionality, include `sb.min.js`  into your `index.html` as well as your own application's js files.
```
<script src="/path/to/sb.min.js"></script>
```

### Set up Switchboard
It is a good idea to set up Switchboard with the details of our application so that we can be assured that we will have all the information that we need when developing our content locally and deploying it to production.

Before any of our application logic we will need the following

```
SB.setup({
	url: "http://xxxx.com.au", // the url to our switchboard
	sources: [ // the data sources in use by our content
		"products.csv",
		"prices.csv",
		"nutrition.csv"
	]
}).then(function () { // called when setup is complete
	// start our content animation
});
```

> **Deprecated** SB.setup supports a success callback as well however this has been deprecated in the latest release.

```
SB.setup({
	url: "http://xxxx.com.au/", // the url to our switchboard
	sources: [ // the data sources in use by our content
		"products.csv",
		"prices.csv",
		"nutrition.csv"
	],
	success: function () { // called when setup is complete
		// start our content animation
	}
});
```

This step will give us access to the content destination's specific data sources and environment variables when developing locally. When the content is deployed to a location the data sources and environment variables will reflect that location rather than the one specified by us.

### Accessing our Data Sources
Accessing data from Switchboard can be done easily via `SB.Data`.

The data from these files can then be accessed using `SB.Data.get` and providing the name of the data source.
```
var products = SB.Data.get("products.csv"); // returns "products.csv"
var prices = SB.Data.get("prices.csv"); // returns "prices.csv"
```

#### Accessing Data Sources Generally
Occasionally we may develop generalised content that may want to adapt based on the data sources assigned to it. In this case we will want to get a data source based on a pattern in the name. For example, we may have a menu that will take any data source with the string `'MenuOptions.csv'` in the name and use that to populate the content.

```
// returns all data sources with name containing 'MenuOptions.csv'
var menuOptions = SB.Data.get().like('MenuOptions.csv');
```

#### Accessing Data for our Data Sources
If we ever need the name or file name of a data source we can get the data from a `meta` attribute assigned to each data source.

```
// get our data source
var products = SB.Data.get("products"); // returns "products.csv"
// access our meta data for that data source
console.log(products.meta.name); // prints "products"
console.log(products.meta.filename); // prints "products.csv"
```

#### The Single method
The `single` method is designed to provide an easy way to access a single result from our pool of data sources. If we know that the result of the `like` or `get` will be a single result this method is guaranteed to return the expected data source. However if more than one data source exists within our pool there is no guarantee that the returned value is the one we expect.

```
MyOtherOptions.csv
MyMenuOptions.csv
MyMenuOptions20160301T050000.csv
MyMenuOptions20160401T060000.csv
```

Using these example data sources the following will occur when using `single()`,

```
// assuming we only have a single data source attached to our content
SB.Data.single(); // returns any of our data sources! Only use if 1 data source exists within the content.
SB.Data.get().single(); // returns the same as SB.Data.single();

SB.Data.like('MenuOptions').single(); // could return any single data source matching 'MenuOptions'
SB.Data.like('MenuOptions.csv').single(); // guaranteed to return 'MyMenuOptions.csv'
SB.Data.like('OtherOptions').single(); // guaranteed to return 'MyOtherOptions.csv'
```

#### Scheduling Data Sources via their Name
In order to schedule content changes automatically it is possible able to append a date in a format resembling [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) just before the '.csv' extension that will be recognised by `scheduled` and return the most recently scheduled data source or the default data source which happens to be the result given by `single`.

The date can look like the following:

```
20160318 - Date
20160318T050000 - Date and Time
```

A date consists of 8 digits in the format YYYYMMDD. A date and time consists of 8 digit date followed by a 'T' and 6 more digits representing a time in the complete format of YYYYMMDDTHHiiss.

For example, we have several data sources attached such as:

```
MyOtherOptions.csv
MyMenuOptions.csv
MyMenuOptions20160301T050000.csv
MyMenuOptions20160401T060000.csv
```

We can retrieve the desired data source based on the current date using `scheduled`.
```
var ourMenuOptions = SB.Data.get().like('MenuOptions'); // only get data sources matching 'MenuOptions'

// if todays date is on or after 5am, 1st March 2016 and before 6am, 1st April 2016
ourMenuOptions.scheduled(); // return MyMenuOptions20160301T050000.csv

// if our date is after 6am, 1st April 2016
ourMenuOptions.scheduled(); // returns MyMenuOptions20160401T060000.csv

// if our date is before 5am, 1st March 2016, i.e. no scheduled menu options
// scheduled() will return the result given by single()
ourMenuOptions.scheduled() == ourMenuOptions.single() // true
```

#### Fetching Data Sources after Setup
There may be a situation in which we would like to load a data source after our initial `SB.setup` has been run. In this situation we have two handle methods, `SB.Data.fetch` and `SB.Data.fetchAll`.

`SB.Data.fetch` is used to retrieve a single data source and takes up to three arguments.

```
var filename = 'basic-products.csv';
var name     = 'products';
var reload   = true;
SB.Data.fetch(filename, name, reload).then(function (data) {
	console.log(data); // our basic-products.csv
	console.log(SB.Data.get('products')); // our basic-products.csv
});
```

If a name is not provided, the data source will need to be referenced by its filename. If `reload` is false, and the data source already exists, no request for the data source will be made and the existing value will be returned.

`SB.Data.fetchAll` is almost exactly the same as `SB.Data.fetch` however it takes two arguments. The first is an array of the data sources we want to load and the second is whether or not we want to reload all the data sources we are fetching. It returns an array of the data sources requested however this will not be in the order specified so using `SB.Data.get` is advised.

```
var dataSources = [
	{ filename: 'basic-products.csv', name: 'products' },
	'basic-nutrition.csv'
];
SB.Data.fetchAll(dataSources, true).then(function () {
	console.log(SB.Data.get('products')); // our newly loaded basic-products.csv
	console.log(SB.Data.get('basic-nutrition.csv')); // our newly loaded basic-nutrition.csv
});
```

> **Note** Just like the `SB.setup`, you can mix and match strings and objects containing name aliases when requesting data sources.

### Joining Data
#### One to One
Now that we have our data we would like to use it for our application. However it's not much use to us having products and prices in separate datasets.

Let's take our two example data sources **products.csv** and **prices.csv**.
```
// products.csv
[
	{ product_code: 1, name: "Beef Burger" },
	{ product_code: 2, name: "Chicken Sandwich" },
	{ product_code: 3, name: "Hot dog" }
]
```
```
// prices.csv
[
	{ product_code: 1, price: 3 },
	{ product_code: 2, price: 4 },
	{ product_code: 3, price: 2 }
]
```

Let's go through and connect the prices data to the products data with the key `product_code` and `SB.Dataset`.

```
var products = SB.Data.get("products.csv"),
	prices   = SB.Data.get("prices.csv");

var productsWithPrices = SB.Dataset(products)
				.join(prices).on('product_code')
				.get();
```

Our variable `productsWithPrices` should now contain

```
[
	{ product_code: 1, name: "Beef Burger", price: 3 },
	{ product_code: 2, name: "Chicken Sandwich", price: 4 },
	{ product_code: 3, name: "Hot dog", price: 2 }
]
```

Ready for use with our application!

Assuming that we have another data source **nutrition.csv** we can chain multiple joins and combine all 3 data sources in a single line.

```
var products  = SB.Data.get("products.csv"),
	prices    = SB.Data.get("prices.csv"),
	nutrition = SB.Data.get("nutrition.csv");

var productsWithTheLot = SB.Dataset(products)
							.join(prices).on('product_code')
							.join(nutrition).on('product_code')
							.get();
```

Now our variable `productsWithTheLot` will look like this

```
[
	{ product_code: 1, name: "Beef Burger", price: 3, kj: 2432 },
	{ product_code: 2, name: "Chicken Sandwich", price: 4, kj: 1799 },
	{ product_code: 3, name: "Hot dog", price: 2, kj: 2301 }
]
```

#### One to Many
We have two data sets, our **products.csv** and an **addons.csv**.
```
// products.csv
[
	{ product_code: 1, name: "Beef Burger" },
	{ product_code: 2, name: "Chicken Sandwich" },
	{ product_code: 3, name: "Hot dog" }
]
```
```
// addons.csv
[
	{ product_code: 1, name: "Cheese", price: .50 },
	{ product_code: 1, name: "Pickles", price: .50 },
	{ product_code: 1, name: "Extra Beef", price: 2 },
	{ product_code: 2, name: "Bacon", price: 1 },
	{ product_code: 3, name: "Onions", price: .50 },
	{ product_code: 3, name: "Sauerkraut", price: 1.50 },
]
```

Let's go and join the add ons data to our products data with `SB.Dataset`'s `joinMany` method which takes the data to join and the name of the property to store the related data on.

```
var productsWithAddons = SB.Dataset(products)
							.joinMany(addons, 'addons').on('product_code')
							.get();
```
Our variable `productsWithAddons` will now be

```
[
	{
		product_code: 1,
		name: "Beef Burger",
		addons: [
			{ product_code: 1, name: "Cheese", price: .50 },
			{ product_code: 1, name: "Pickles", price: .50 },
			{ product_code: 1, name: "Extra Beef", price: 2 }
		]
	},
	...
]
```

#### Different Keys
We don't need to have our data sources containing the same property to join them as seen in the above examples. We can join our sources with different key names by providing `on()` with two arguments, the first being our primary data sources property name and the second being the joining data sources property name.

Let's take our two example data sources **products.csv** and **prices.csv**.
```
// products.csv
[
	{ code: 1, name: "Beef Burger" },
	{ code: 2, name: "Chicken Sandwich" },
	{ code: 3, name: "Hot dog" }
]
```
```
// prices.csv
[
	{ product_code: 1, price: 3 },
	{ product_code: 2, price: 4 },
	{ product_code: 3, price: 2 }
]
```

```
var productsWithPrices = SB.Dataset(products)
							.join(prices).on('code', 'product_code')
							.get();
```

### Filtering Data
Often we need to get only a few select items from a collection. Instead of writing
a for loop everytime we need to do that `SB.Dataset` comes with a helpful method for filtering, i.e. `where`.

Let's take our products with prices dataset and only get the information for product with code 1.

```
// [{ code: 1, name: "Beef Burger", ... }]
var product1WithPrice = SB.Dataset(productsWithPrices)
													.where('code', '=', 1);
```

Alternative operators that can be used with `where` include `>`, `>=`, `<` and `<=`.

### Storing Data
Since we have a useful collection of products containing all the data we need we may want to store it for future use within our content. By using `SB.Data.set` we can store new data or overwrite our old data with our fresh easy-to-use data.

```
SB.Data.get("products.csv"); // returns original data
SB.Data.set("products.csv", productsWithTheLot); // overwrite products.csv
SB.Data.get("products.csv"); // returns productsWithTheLot
```

### Getting Environment Data
> **Note:** Mocking environment data locally is now implemented

`SB.Env` provides a couple of methods for retrieving environment data via some properties of  `SB.Env` or by using its helpful `get()` method.

Using the former, we can access the following data using `SB.Env`.

```
// Basic Location Data
SB.Env.location;
/**
{
	id: "Test",
    name: "New Location 1",
    label: "Red",
    timezone: "Australia/Sydney",
    groups: [
	    "location-group-1",
     	"location-group-2"
    ]
}
**/

// Screen Orientation
SB.Env.orientation; // 'vertical' or 'horizontal'

// Sequence (Screen relative position to other screens)
SB.Env.sequence; // A number between 1 - 6

// Basic Content Data
SB.Env.content;
/**
{
	name:"basic-coffee.zip",
    label:"Red",
    modified: "2016-01-17T19:18:54.000Z",
    groups: [
     	"my-group",
     	"my-test-group"
    ]
}
**/

// Basic Channel Data
SB.Env.content;
/**
{
	name: "My channel name",
    screen: "",
    screens: [],
    groups: []
}
**/
```

To access any other environment data that wasn't mentioned above we can use the
`SB.Env.get()` method, providing it with a string indicating the data that we would like to retrieve.

For example, to get `content-name` we will use
```
SB.Env.get('content-name'); // 'basic-coffee.zip'
```

Not providing a name to the call will retrieve all Environment data
```
SB.Env.get(); // All Environment Data
/**
{
	"location": "Test",
	"location-name": "New Location 1",
	"location-label": "Red",
	"location-timezone": "Australia/Sydney",
	...
	"channel-screens": [],
	"channel-groups": []
}
**/
```

#### Mocking environment data from local

By including a couple additional details about our application to `SB.setup` we can mock location environment data for local development.

These additional and required details include `environmentLocation` and `content`.

```
SB.setup({
	url: "http://xxxx.com.au/", // the url to our switchboard
	environmentLocation: "DEMO",
	content: "my-content.zip",
	success: function () { // called when setup is complete
		// start our content animation
	}
});
```

For even more detailed channel and screen data it is recommended that you provide a `channelScreenId` *or* a `screenId`.

The `channelScreenId` can be found on the HQ Scheduler, inspect the screens at the top for the scheduler-screen div's data-id property.
The `screenId` can be found on the Local Device Assigner, inspect the assigned screen for the assigned-screen div's data-id property.

```
SB.setup({
	url: "http://xxxx.com.au/", // the url to our switchboard
	environmentLocation: "DEMO",
	content: "my-content.zip",
	channelScreenId: 1, // OR
	screenId: 1,
	success: function () { // called when setup is complete
		// start our content animation
	}
});
```
----

## The Timeline
`Timeline` exists as a tool to assist in the coordination and execution of events synchronised to the system clock that repeats on a cycle.

### Creating our Timeline
We are able to run multiple timelines at once however for most applications we should only need a single instance. The `Timeline` constructor takes an object with options for customising timeline to your needs.
```
var timeline = new SB.Timeline({
	debug: true, // truthy value, will visualise the timeline
	framerate: 30, // frames per second, default: 25
	cycleTime: 60000, // milliseconds, length of the timeline cycle, default: 0
	offset: 40 // milliseconds, offsets system clock, default: 0
});
```

Once we have done this we will have a running timeline that repeats every 60,000 milliseconds (every minute) at 30fps.

> **Note:** For smoother animations increase the framerate value at the expense of performance.

> **Another Note:** The timeline debugger is currently limited in features however you can view your entire timeline, the two types of events being run and what time that particular event is executing.

### Adding a Fixed Event
Adding an 'event' to your timeline is done using `addEvent`. Lets add an event to our newly created timeline that runs once at the beginning and congratulates us on creating a new timeline and an event to keep it company.

```
timeline.addEvent(function () {
	console.log("Well done, you are the best!");
	console.log(new Date().getMinutes());  // the current minute
}, 0);
```

Run this code and you will see that our timeline will print that message every minute on the minute. Even though it only triggers once at 0ms, due to the nature of the timeline we loop through that event again once the cycle completes.

### Adding an Interval Event
Lets add another event that triggers multiple times within a cycle using the same method.

```
timeline.addEvent(function () {
	console.log("I am going to happen 6 times a cycle!");
}, 0, 10000);
```

As stated in the log, this event will run every 10,000 milliseconds from 0ms. If we wanted to only run this event in the last half of the cycle we could replace the second parameter `0` with `30000` and have the event only trigger 3 times each cycle.

```
timeline.addEvent(function () {
	console.log("I am going to happen 3 times a cycle!");
}, 30000, 10000);
```

### Triggering the Latest Event
Often when we build our timeline we want an event to occur so that something is shown such as the 1st item in a menu or the 1st slide in a rotator. However if our content loads halfway through our timeline it can be jarring for the audience to see a menu skip from our 1st item to our 4th or 5th item. To get around this, `SB.Timeline` has a helper method, `timeline.triggerLatest()`. This runs the latest event in the timeline so that the initial state of the content reflects the correct state it would be in as if it had been running forever.

For example,

```
var timeline = new SB.Timeline({ cycleTime: 40000 });

timeline.addEvent(function () { showItem(1); }, 0);
timeline.addEvent(function () { showItem(2); }, 10000);
timeline.addEvent(function () { showItem(3); }, 20000);
timeline.addEvent(function () { showItem(4); }, 30000);

// if timeline.cyclePosition === 5000
timeline.triggerLatest(); // focus on our 1st item

// if timeline.cyclePosition === 20000
timeline.triggerLatest(); // focus on our 3rd item

// if timeline.cyclePosition === 29999
timeline.triggerLatest(); // focus on our 3rd item

// if timeline.cyclePosition === 30001
timeline.triggerLatest(); // focus on our 4th item
```

### Clear the Timeline
If for any reason we wanted to clear all events from the Timeline and start over we only need to use `timeline.clear()`.

### Resetting the Timeline
There are often times when you would like content to play from the beginning such as when showing content again after a takeover. It works by setting the offset of the timeline to the current time, effectively starting the timeline from the 0th frame.

```
timeline.reset();
```

### Debugging the Timeline
A brief explanation of the visual debugger.

![Visual Debugger](http://imgur.com/tlXGpp7.png)

The gray bar is our timeline.
The light gray bar that increases in size is the current progress of our timeline.

The top row of blue *pegs* are our interval events.
The bottom row of orange *pegs* are our fixed events.

#### Tagging an Event
Hovering over these blue and orange event *pegs* will display some text in the top left of the debugger. This text describes the events *tag* and the time it will trigger. To tag an event for identification we provide a string as our 4th parameter to the `addEvent` calls like so,

```
// Tagging a fixed event
timeline.addEvent(function () {
	console.log("Well done, you are the best!");
	console.log(new Date().getMinutes());  // the current minute
}, 0, null, "My First Event!");

// Tagging an interval event
timeline.addEvent(function () {
	console.log("I am going to happen 6 times a cycle!");
}, 0, 10000, "My First Interval Event!");
```

----

## Examples
You can find examples of generic Switchboard content with our framework in the `switchboard-content` repository.

----

## Development Guide

This project is designed to be a collaborative work-in-progress by any and all content developers. The following is a brief guide to get started with development.

###Technologies
The content framework uses ES6 and Babel to create a cross platform library that still lets developers use the newest features available through ES6.

For unit testing, karma and jasmine are used.

### Development
To start development you will need to have node, npm and gulp installed.

Once you have those 3 programs, run the following to start development.

```
npm i
gulp
```

This will install the gulp and testing dependencies you need and run the default task which watches the source and builds it when it detects any changes.

### Testing
To unit test, run the following.

```
npm test
```

This will watch for any changes in the build folder and run the test suite.

### Adding Changes to Master
To add the work you have done into the Master branch you will need to create a pull request with the feature and accompanying tests to be reviewed by the project administrator.
